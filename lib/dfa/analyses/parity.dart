import 'package:sca22/dfa/analysis.dart';
import 'package:sca22/cfg/cfg.dart';

enum Parity { Odd, Even }

class ParityAnalysis
    extends IntraproceduralForwardAnalysis<Map<Variable, Parity>> {
  @override
  final initElement = {};
  @override
  final oneElement = null;

  ParityAnalysis(IntraproceduralControlFlowGraph cfg) : super(cfg);

  @override
  bool equal(Map<Variable, Parity> left, Map<Variable, Parity> right) =>
      left == right ||
      (left != oneElement &&
          right != oneElement &&
          left.length == right.length &&
          left.entries.every((entry) =>
              right.containsKey(entry.key) && right[entry.key] == entry.value));

  @override
  Map<Variable, Parity> merge(
          Map<Variable, Parity> left, Map<Variable, Parity> right) =>
      left == oneElement
          ? right
          : right == oneElement
              ? left
              : Map.fromEntries(left.entries.where((entry) =>
                  right.containsKey(entry.key) &&
                  right[entry.key] == entry.value));

  @override
  Map<Variable, Parity> apply(Node node, Map<Variable, Parity> incoming) {
    if (node is VariableDefinition &&
        incoming != oneElement &&
        evaluate(node, incoming) is Parity) {
      return Map.fromEntries({MapEntry(node.variable, evaluate(node, incoming))}
          .union(incoming.entries
              .where((entry) => entry.key != node.variable)
              .toSet()));
    } else if (node is VariableDefinition &&
        incoming != oneElement &&
        evaluate(node, incoming) is! Parity) {
      return Map.fromEntries(
          incoming.entries.where((entry) => entry.key != node.variable));
    } else {
      return incoming;
    }
  }

  Parity evaluate(Node node, Map<Variable, Parity> environment) {
    if (node is BinaryOperation) {
      final right = lookup(node.right, environment);
      final left = lookup(node.left, environment);
      switch (node.runtimeType) {
        case Subtract:
        case Add:
          return right is Parity && left is Parity
              ? right == left
                  ? Parity.Even
                  : Parity.Odd
              : null;
        case Multiply:
          return right == Parity.Even || left == Parity.Even
              ? Parity.Even
              : right is Parity && left is Parity
                  ? Parity.Odd
                  : null;
        default:
          return null;
      }
    } else if (node is Minus) {
      return lookup(node.operand, environment);
    } else if (node is SimpleAssignment) {
      return lookup(node.value, environment);
    } else {
      return null;
    }
  }

  Parity lookup(Operand operand, Map<Variable, Parity> environment) {
    if (operand is Variable && environment.containsKey(operand)) {
      return environment[operand];
    } else if (operand is Number) {
      return operand.value % 2 == 0 ? Parity.Even : Parity.Odd;
    } else {
      return null;
    }
  }
}
