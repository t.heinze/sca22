import 'package:sca22/dfa/analysis.dart';
import 'package:sca22/cfg/cfg.dart';

class DominatorAnalysis extends IntraproceduralForwardAnalysis<Set<int>> {
  @override
  final initElement;
  @override
  final oneElement;
  DominatorAnalysis(IntraproceduralControlFlowGraph cfg)
      : oneElement = cfg.nodes.map((node) => node.id).toSet(),
        initElement = {cfg.entryNode.id},
        super(cfg);
  @override
  bool equal(Set<int> left, Set<int> right) =>
      left.length == right.length && left.every((i) => right.contains(i));
  @override
  Set<int> merge(Set<int> left, Set<int> right) => left.intersection(right);
  @override
  Set<int> apply(Node node, Set<int> incoming) => incoming.union({node.id});
}
