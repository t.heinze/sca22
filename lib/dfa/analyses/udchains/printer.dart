import 'package:sca22/dfa/analyses/definitions.dart';
import 'package:sca22/cfg/printer.dart';
import 'package:sca22/cfg/cfg.dart';

class UDChainsDotPrinter extends CFGDotPrinter {
  Map<Node, Set<Definition>> udchains;

  @override
  String print(IntraproceduralControlFlowGraph cfg,
      {Map<Node, Set<Definition>> udchains = const {}}) {
    this.udchains = udchains;
    return super.print(cfg);
  }

  @override
  String visitNode(Node node) {
    return super.visitNode(node) +
        udchains[node]
            .map((definition) => '${node.id}->${definition.node.id}'
                '[fontcolor=red,color=red,style=dashed,'
                'label="${definition.variable}"]')
            .join(';');
  }
}
