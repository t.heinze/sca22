class UnionFind<T> {
  final parent = <T, T>{};

  void makeSet(T x) => parent.putIfAbsent(x, () => x);

  T find(T x) {
    if (x != parent[x]) parent[x] = find(parent[x]);
    return parent[x];
  }

  void union(T x, T y) {
    assert(x == parent[x] && y == parent[y] && x != y);
    parent[x] = y;
  }
}
