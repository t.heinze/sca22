import 'package:sca22/ast/visitor.dart';
import 'package:sca22/util/log.dart';
import 'package:sca22/ast/ast.dart';
import 'unionfind.dart';
import 'types.dart';

class TypeCheckerException implements Exception {}

class UnionFindSolver extends UnionFind<Term> {
  void unify(Term x, Term y) {
    makeSet(x);
    makeSet(y);
    x = find(x);
    y = find(y);
    if (x != y) {
      if (x is TypeVariable && y is TypeVariable) {
        union(x, y);
      } else if (x is TypeVariable && y is Term) {
        union(x, y);
      } else if (x is Term && y is TypeVariable) {
        union(y, x);
      } else if (x is FunctionType &&
          y is FunctionType &&
          x.parameterTypes.length == y.parameterTypes.length) {
        union(x, y);
        for (var i = 0; i < x.parameterTypes.length; i++) {
          unify(x.parameterTypes[i], y.parameterTypes[i]);
        }
        unify(x.returnType, y.returnType);
      } else if (x is ArrayType && y is ArrayType) {
        union(x, y);
        unify(x.elementType, y.elementType);
      } else {
        throw TypeCheckerException();
      }
    }
  }
}

/// A unification-based type checker for the WHILE language.
class TypeChecker extends Visitor<void> {
  FunctionDeclaration currentFunction; // maybe null
  UnionFindSolver solver;
  Map typeVariables;

  TypeVariable typeVariable(Node node) => typeVariables.putIfAbsent(
      node is Identifier ? node.name : node, () => ExpressionVariable(node));

  void unify(Term x, Term y) {
    log.fine('Typechecking: New type constraint $x = $y');
    solver.unify(x, y);
  }

  // TODO: add type annotations, dynamic type, and gradual typing
  // TODO: support non-disjunct variable names
  // TODO: add parametric polymorphism

  void typecheck(Program ast) {
    solver = UnionFindSolver();
    typeVariables = {};
    // predefined functions: [[scan]] = ()->int /\ [[print]] = (int)->int
    // main function: [[main]] = ()->int
    unify(typeVariable(Identifier('main')), FunctionType([], IntegerType()));
    unify(typeVariable(Identifier('scan')), FunctionType([], IntegerType()));
    unify(typeVariable(Identifier('print')),
        FunctionType([IntegerType()], IntegerType()));
    visitProgram(ast);
  }

  @override
  void visitProgram(Program node) =>
      node.functions.forEach((func) => func.accept(this));
  @override
  void visitBlock(Block node) =>
      node.statements.forEach((stmt) => stmt.accept(this));

  // [[name]] = (...,[[parameter]],...)->[[currentFunction]]
  @override
  void visitFunctionDeclaration(FunctionDeclaration node) {
    currentFunction = node;
    node.body.accept(this);
    unify(
        typeVariable(node.name),
        FunctionType([
          for (var parameter in node.parameters.list) typeVariable(parameter)
        ], typeVariables.putIfAbsent(currentFunction, () => TypeVariable())));
  }

  // [[condition]] = int
  @override
  void visitIfElseStatement(IfElseStatement node) {
    node.condition.accept(this);
    node.ifBranch.accept(this);
    node.elseBranch?.accept(this);
    unify(typeVariable(node.condition), IntegerType());
  }

  // [[condition]] = int
  @override
  void visitWhileStatement(WhileStatement node) {
    node.condition.accept(this);
    node.body.accept(this);
    unify(typeVariable(node.condition), IntegerType());
  }

  @override
  // [[target = source]] = [[source]]
  // [[target]] = [[source]]
  void visitAssignment(Assignment node) {
    node.source.accept(this);
    node.target.accept(this);
    unify(typeVariable(node), typeVariable(node.source));
    unify(typeVariable(node.target), typeVariable(node.source));
  }

  // [[value]] = [[currentFunction]]
  @override
  void visitReturnStatement(ReturnStatement node) {
    node.value.accept(this);
    unify(typeVariable(node.value),
        typeVariables.putIfAbsent(currentFunction, () => TypeVariable()));
  }

  // [[left + right]] = int
  // [[left - right]] = int
  // [[left * right]] = int
  // [[left / right]] = int
  // [[left > right]] = int
  // [[left < right]] = int
  // [[left]] = [[right]]
  // [[left]] = int
  @override
  void visitBinaryExpression(BinaryExpression node) {
    node.left.accept(this);
    node.right.accept(this);
    unify(typeVariable(node.left), typeVariable(node.right));
    unify(typeVariable(node.left), IntegerType());
    unify(typeVariable(node), IntegerType());
  }

  // [[left == right]] = int
  // [[left]] = [[right]]
  @override
  void visitEqual(Equal node) {
    node.left.accept(this);
    node.right.accept(this);
    unify(typeVariable(node.left), typeVariable(node.right));
    unify(typeVariable(node), IntegerType());
  }

  // [[left != right]] = int
  // [[left]] = [[right]]
  @override
  void visitNotEqual(NotEqual node) {
    node.left.accept(this);
    node.right.accept(this);
    unify(typeVariable(node.left), typeVariable(node.right));
    unify(typeVariable(node), IntegerType());
  }

  // [[-operand]] = int
  // [[!operand]] = int
  // [[operand]] = int
  @override
  void visitUnaryExpression(UnaryExpression node) {
    node.operand.accept(this);
    unify(typeVariable(node.operand), IntegerType());
    unify(typeVariable(node), IntegerType());
  }

  // [[callee]] = (...,[[argument]],...)->[[node]]
  @override
  void visitCall(Call node) {
    node.arguments.list.forEach((arg) => arg.accept(this));
    node.callee.accept(this);
    unify(
        typeVariable(node.callee),
        FunctionType(
            [for (var argument in node.arguments.list) typeVariable(argument)],
            typeVariable(node)));
  }

  // [[number]] = int
  @override
  void visitNumber(Number node) {
    unify(typeVariable(node), IntegerType());
  }

  @override
  void visitArray(Array node) {
    node.values.list.forEach((arg) => arg.accept(this));
    var alpha = TypeVariable();
    var newArrayType = ArrayType(alpha);
    unify(typeVariable(node), newArrayType);
    if (node.values.list.isNotEmpty) {
      unify(alpha, typeVariable(node.values.list.first));
    }
    // TODO: weiter implementieren .. es fehlen
    // auch noch die Regeln für IndexExpression
  }
}
