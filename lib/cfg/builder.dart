import 'package:sca22/ast/visitor.dart' as ast;
import 'package:sca22/ast/ast.dart' as ast;
import 'cfg.dart';

class IntraproceduralControlFlowGraphImpl
    extends IntraproceduralControlFlowGraph {
  final Set<Node> entryNodes, exitNodes;
  Set<Node> _nodes;

  IntraproceduralControlFlowGraphImpl(this.entryNodes, this.exitNodes);

  factory IntraproceduralControlFlowGraphImpl.empty() =>
      IntraproceduralControlFlowGraphImpl({}, {});
  factory IntraproceduralControlFlowGraphImpl.single(Node node) =>
      IntraproceduralControlFlowGraphImpl({node}, {node});

  @override
  Node get entryNode => entryNodes.first;
  @override
  Node get exitNode => exitNodes.first;
  @override
  Set<Node> get nodes =>
      _nodes ??
      (_nodes =
          entryNodes.fold({}, (value, node) => value.union(collect(node, {}))));

  Set<Node> collect(Node node, Set<Node> visited) {
    if (!visited.contains(node)) {
      visited.add(node);
      for (var successor in node.successors) {
        collect(successor, visited);
      }
    }
    return visited;
  }

  IntraproceduralControlFlowGraphImpl append(
      IntraproceduralControlFlowGraphImpl other) {
    if (other.entryNodes.isEmpty && other.exitNodes.isEmpty) {
      return this;
    } else if (entryNodes.isEmpty && exitNodes.isEmpty) {
      return other;
    } else {
      other.entryNodes.forEach((node) => node.predecessors.addAll(exitNodes));
      exitNodes.forEach((node) => node.successors.addAll(other.entryNodes));
      return IntraproceduralControlFlowGraphImpl(entryNodes, other.exitNodes);
    }
  }

  IntraproceduralControlFlowGraphImpl merge(
          IntraproceduralControlFlowGraphImpl other) =>
      IntraproceduralControlFlowGraphImpl(
          entryNodes.union(other.entryNodes), exitNodes.union(other.exitNodes));
}

class ExpressionBuilder extends ast.ExpressionVisitor<Operand> {
  var graph = IntraproceduralControlFlowGraphImpl.empty();

  BinaryOperation createForBinaryExpression(
      Type type, Variable variable, Operand left, Operand right) {
    BinaryOperation node;
    switch (type) {
      case ast.Less:
        node = Less(variable, left, right);
        break;
      case ast.Greater:
        node = Greater(variable, left, right);
        break;
      case ast.Equal:
        node = Equal(variable, left, right);
        break;
      case ast.NotEqual:
        node = NotEqual(variable, left, right);
        break;
      case ast.Add:
        node = Add(variable, left, right);
        break;
      case ast.Subtract:
        node = Subtract(variable, left, right);
        break;
      case ast.Multiply:
        node = Multiply(variable, left, right);
        break;
      case ast.Divide:
        node = Divide(variable, left, right);
        break;
    }
    return node;
  }

  UnaryOperation createForUnaryExpression(
      Type type, Variable variable, Operand operand) {
    UnaryOperation node;
    switch (type) {
      case ast.Minus:
        node = Minus(variable, operand);
        break;
      case ast.Not:
        node = Not(variable, operand);
        break;
    }
    return node;
  }

  @override
  Operand visitAssignment(ast.Assignment node) {
    final target = node.target;
    final variable =
        target is ast.Identifier ? Variable(target.name) : VirtualVariable();
    if (target is ast.IndexExpression) {
      final source = node.source.accept(this);
      final array = target.array.accept(this);
      final index = target.index.accept(this);
      final statement = ArrayWrite(variable, array, index, source);
      graph =
          graph.append(IntraproceduralControlFlowGraphImpl.single(statement));
    } else {
      final source = node.source;
      Node statement;
      if (source is ast.BinaryExpression) {
        final left = source.left.accept(this);
        final right = source.right.accept(this);
        statement = createForBinaryExpression(
            source.runtimeType, variable, left, right);
      } else if (source is ast.UnaryExpression) {
        final operand = source.operand.accept(this);
        statement =
            createForUnaryExpression(source.runtimeType, variable, operand);
      } else if (source is ast.Call) {
        final arguments = source.arguments.list
            .map((argument) => argument.accept(this))
            .toList();
        final receiver = source.callee.accept(this);
        statement = Call(variable, receiver, arguments);
      } else if (source is ast.Array) {
        statement = Allocate(variable);
      } else if (source is ast.IndexExpression) {
        final array = source.array.accept(this);
        final index = source.index.accept(this);
        statement = ArrayRead(variable, array, index);
      } else {
        statement = SimpleAssignment(variable, source.accept(this));
      }
      graph =
          graph.append(IntraproceduralControlFlowGraphImpl.single(statement));
      if (source is ast.Array) {
        var i = 0;
        source.values.list.forEach((value) {
          final initvalue = value.accept(this);
          final initializer =
              ArrayWrite(VirtualVariable(), variable, Number(i++), initvalue);
          graph = graph
              .append(IntraproceduralControlFlowGraphImpl.single(initializer));
        });
      }
    }
    return variable;
  }

  @override
  Operand visitBinaryExpression(ast.BinaryExpression node) {
    final variable = VirtualVariable();
    final left = node.left.accept(this);
    final right = node.right.accept(this);
    final statement =
        createForBinaryExpression(node.runtimeType, variable, left, right);
    graph = graph.append(IntraproceduralControlFlowGraphImpl.single(statement));
    return variable;
  }

  @override
  Operand visitUnaryExpression(ast.UnaryExpression node) {
    final variable = VirtualVariable();
    final operand = node.operand.accept(this);
    final statement =
        createForUnaryExpression(node.runtimeType, variable, operand);
    graph = graph.append(IntraproceduralControlFlowGraphImpl.single(statement));
    return variable;
  }

  @override
  Operand visitArray(ast.Array node) {
    final variable = VirtualVariable();
    final statement = Allocate(variable);
    var i = 0;
    graph = graph.append(IntraproceduralControlFlowGraphImpl.single(statement));
    node.values.list.forEach((value) {
      final initvalue = value.accept(this);
      final initializer =
          ArrayWrite(VirtualVariable(), variable, Number(i++), initvalue);
      graph =
          graph.append(IntraproceduralControlFlowGraphImpl.single(initializer));
    });
    return variable;
  }

  @override
  Operand visitCall(ast.Call node) {
    final variable = VirtualVariable();
    final arguments =
        node.arguments.list.map((argument) => argument.accept(this)).toList();
    final receiver = node.callee.accept(this);
    final statement = Call(variable, receiver, arguments);
    graph = graph.append(IntraproceduralControlFlowGraphImpl.single(statement));
    return variable;
  }

  @override
  Operand visitIndexExpression(ast.IndexExpression node) {
    final variable = VirtualVariable();
    final array = node.array.accept(this);
    final index = node.index.accept(this);
    final statement = ArrayRead(variable, array, index);
    graph = graph.append(IntraproceduralControlFlowGraphImpl.single(statement));
    return variable;
  }

  @override
  Operand visitIdentifier(ast.Identifier node) => Variable(node.name);
  @override
  Operand visitNumber(ast.Number node) => Number(node.value);
}

class Builder extends ast.Visitor<IntraproceduralControlFlowGraphImpl> {
  Builder();

  Map<String, IntraproceduralControlFlowGraph> build(ast.Program ast) => {
        for (final function in ast.functions)
          function.name.name: function.accept(this)
      };

  @override
  IntraproceduralControlFlowGraphImpl visitFunctionDeclaration(
      ast.FunctionDeclaration node) {
    VirtualVariable.counter = 1;
    Node.counter = 0;
    return IntraproceduralControlFlowGraphImpl.single(Entry())
        .append(node.body.accept(this))
        .append(IntraproceduralControlFlowGraphImpl.single(Exit()));
  }

  @override
  IntraproceduralControlFlowGraphImpl visitBlock(ast.Block node) =>
      node.statements.fold(IntraproceduralControlFlowGraphImpl.empty(),
          (fragment, statement) => fragment.append(statement.accept(this)));

  @override
  IntraproceduralControlFlowGraphImpl visitIfElseStatement(
      ast.IfElseStatement node) {
    final builder = ExpressionBuilder();
    final condition = Branch(node.condition.accept(builder));
    final branching = builder.graph
        .append(IntraproceduralControlFlowGraphImpl.single(condition));
    var fragment = node.ifBranch.accept(this);
    fragment = branching.append(fragment);
    var other = node.elseBranch?.accept(this) ??
        IntraproceduralControlFlowGraphImpl.empty();
    other = branching.append(other);
    fragment = fragment.merge(other);
    return fragment;
  }

  @override
  IntraproceduralControlFlowGraphImpl visitWhileStatement(
      ast.WhileStatement node) {
    final builder = ExpressionBuilder();
    final condition = Branch(node.condition.accept(builder));
    final branching = builder.graph
        .append(IntraproceduralControlFlowGraphImpl.single(condition));
    var loop = node.body.accept(this);
    loop = branching.append(loop.append(branching));
    return loop;
  }

  @override
  IntraproceduralControlFlowGraphImpl visitExpression(ast.Expression node) {
    final builder = ExpressionBuilder();
    if (node is ast.Identifier || node is ast.Number) {
      final statement =
          SimpleAssignment(VirtualVariable(), node.accept(builder));
      return IntraproceduralControlFlowGraphImpl.single(statement);
    } else {
      node.accept(builder);
      return builder.graph;
    }
  }

  @override
  IntraproceduralControlFlowGraphImpl visitReturnStatement(
      ast.ReturnStatement node) {
    final builder = ExpressionBuilder();
    final statement = Return(node.value.accept(builder));
    return builder.graph
        .append(IntraproceduralControlFlowGraphImpl.single(statement));
  }
}
